package de.saftschuppen.backpack;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
    public static String PrefixChat = ChatColor.GRAY + "[" + ChatColor.GREEN + "Backpack" + ChatColor.GRAY + "] ";

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(new Backpack(this), this);
    }

    @Override
    public void onDisable() {
    }

    @EventHandler
    public void PlayerJoin(PlayerJoinEvent event) {
    }
}
